**Graphite structure**

The initial structure of graphite. The structure is taken from the website (http://rruff.geo.arizona.edu/AMS/minerals/Graphite) from the article " Crystal structures 1 (1963) 7-83 second edition. Interscience Publishers, New York, NY" as a cif-file. Then, the cif-file was converted to the quantum espresso format.

QE input file:

```
&CONTROL
  calculation = 'relax'
  forc_conv_thr =   1.00d-04
  prefix = 'C'
  pseudo_dir = './pseudo/'
/
&SYSTEM
  ecutrho =   3.6000000000d+02
  ecutwfc =   4.5000000000d+01
  ibrav = 0
  nat = 4
  nosym = .false.
  ntyp = 1
/
&ELECTRONS
  conv_thr =   8.0000000000d-10
  electron_maxstep = 80
  mixing_beta =   4.0000000000d-01
/
&IONS
 !
/
&CELL
 cell_dofree = 'all'
/
ATOMIC_SPECIES
C      12.0107 C.pbe-n-kjpaw_psl.1.0.0.UPF
ATOMIC_POSITIONS crystal
C            0.0000000000       0.0000000000       0.0000000000 
C            0.0000000000       0.0000000000       0.5000000000 
C            0.6666666667       0.3333333333       0.0000000000 
C            0.3333333333       0.6666666667       0.5000000000 
K_POINTS automatic
10 10 4 0 0 0
CELL_PARAMETERS angstrom
     -1.2280000000      -2.1269583917      -0.0000000000
     -1.2280000000       2.1269583917       0.0000000000
      0.0000000000       0.0000000000      -6.6960000000
```

```
Begin final coordinates

ATOMIC_POSITIONS (crystal)
C            -0.0000000000        0.0000000000        0.0000000000
C             0.0000000000        0.0000000000        0.5000000000
C             0.6666666667        0.3333333333       -0.0000000000
C             0.3333333333        0.6666666667        0.5000000000
End final coordinates
```

The positions of the atoms haven't changed after relaxation. We'll use this structure for further calculations.

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/InputData/graphiteStruct.png" alt="graphiteStruct" style="zoom:67%;" />

**Convergence w.r.t. the kinetic energy cutoff**

```
 &CONTROL
   calculation = 'scf',
   ...
 /
 &SYSTEM
   ibrav = 0
   nat = 4
   nosym = .false.
   ntyp = 1
   ecutrho = 3.600d+02    
   ecutwfc = 60, 
 /
 ...
```

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/1.ECutWfc/graphite.ecuwfc.png" alt="graphite.ecuwfc" style="zoom:85%;" />

Convergence achieved for the 40 Ry. 

**Convergence w.r.t. k-points**

```
 &CONTROL
   calculation = 'scf',
   prefix='graphite',
   pseudo_dir = './pseudo/'
 /
 ...
 K_POINTS automatic
   8 8 8 0 0 0
 ...
```

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/2.KPoints/graphite.kPoints.png" alt="graphite.kPoints" style="zoom:85%;" />

Convergence achieved for "7 7 7 0 0 0 " - 32 automatically generated k-points.

**Вычисление объемного модуля упругости**

Объемный модуль упругости K:

$ K = -V_0 \frac{dP}{dV} = V_0 \left. \frac{d^2E}{dV^2} \right|_{T=0}$ 

1) При малом отклонении от положения равновесия имеем зависимость: $ E = E_0 + B_1V +B_2 V^2$

Тогда:

$ V_0 = - \frac{B_1}{2B_2}, \;\;\; K = V_02B_2 = - B_1$

Из рачетов получаем, что $K = 71$ GPa, $ V_0 = 35.6 A^3$ . Посмотрим также на график.

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/3.EqOfState/E_V.png" alt="E_V" style="zoom:50%;" />

2) Также произведем расчет используя утилиту QE ev.x. Используем Murnaghan уравнение состояния.

```
     Lattice parameter or Volume are in (au, Ang) > au
     Enter type of bravais lattice (fcc, bcc, sc, noncubic) > noncubic
     Enter type of equation of state :
     1=birch1, 2=birch2, 3=keane, 4=murnaghan > 4
     ...
```

Получим, что $K = 69 \pm 4$  GPa, $ V_0 = 35.57 A^3$ .

3)  Статья "Density functional study of graphite bulk and surface properties" by Newton Ooi , Asit Rairkar, James B. Adams, https://doi.org/10.1016/j.carbon.2005.07.036. В статье приведены следующие значения для графита.

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/ReferenceArticles/jcarbon2005_bulkTable.png" alt="jcarbon2005_bulkTable" style="zoom:80%;" />

Видно, что значения расчитанные мной и приведенные в статье отличаются примерно в 2 раза. 

**Band Structure**

Выберем путь M-Г-K-M в зоне Бриллюэна. Это позволит сопоставиться со статьей: "Dynamics of Carriers and Photoinjected Currents in Carbon Nanotubes and Graphene" Ryan W. Newson, PhD Thesis. 

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/4.BandStructure/PathInRecSpace.png" alt="PathInRecSpace" style="zoom:80%;" />

Сначала выполнил самосогласованный расчет. 

```
 &CONTROL
   calculation = 'bands'
 /
 &SYSTEM
   ibrav = 0 
   celldm(1) = 4.641167  
   nat = 4
   nosym = .false.
   ntyp = 1
   ecutrho = 3.600d+02    
   ecutwfc = 40
   nbnd = 20 
 /
...
K_POINTS crystal_b
           4       
        0.0000000000    0.5000000000    0.0000000000    50 !M
        0.0000000000    0.0000000000    0.0000000000    50 !Gamma
        0.3333300000    0.3333300000    0.0000000000    50 !K
        0.0000000000    0.5000000000    0.0000000000    0  !M
...
```

Далее используем утилиту bands.x и построим то, как выглядит зонная структура. 

```
 &BANDS
   prefix='graphite',
   filband = 'graphite.bands.dat'
   lsym = .true
/
```

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/4.BandStructure/graphite.bands.png" alt="graphite.bands" style="zoom: 40%;" />

Band Structure from the article. 

<img src="/home/timofey/Coding/Python/NumericalMethodsInQM/5-6.EqOfState_ZoneStructure/Calculations/4.BandStructure/Graphite_Reference.png" alt="Graphite_Reference" style="zoom: 80%;" />

Видно, что полученные зонные структуры похожи. Однако для K точки на уровне Ферми наблюдаеются различия. Стоит попробовать другие обменно-корреляционные функционалы и другой псевдопотенциал, чтобы получить более точный результат. 

