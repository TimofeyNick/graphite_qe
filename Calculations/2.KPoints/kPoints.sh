#!/bin/sh
# reminder: from now on, what follows the character # is a comment


# delete the graphite.etot_vs_ecut if it exists
rm -f graphite.etot_vs_ecut

# loop over ecutwfc value
for kpoints in 2 3 4 5 6 7 8 #6 8 10 12
do
    echo "Running for kpoints = $kpoints $kpoints $kpoints ..."

    # self-congraphitestent calculation
    cat > pw.graphite.scf.in << EOF
 &CONTROL
   calculation = 'scf',
   prefix='graphite',
   pseudo_dir = './pseudo/'
 /
 &SYSTEM
   ibrav = 0
   nat = 4
   nosym = .false.
   ntyp = 1
   ecutrho = 3.600d+02    
   ecutwfc = 40, 
 /
 &ELECTRONS
 /
ATOMIC_SPECIES
   C 12.0107 C.pbe-n-kjpaw_psl.1.0.0.UPF
ATOMIC_POSITIONS crystal
   C  0.0000000000       0.0000000000       0.0000000000
   C  0.0000000000       0.0000000000       0.5000000000
   C  0.6666666667       0.3333333333       0.0000000000
   C  0.3333333333       0.6666666667       0.5000000000
K_POINTS automatic
   $kpoints $kpoints $kpoints 0 0 0
CELL_PARAMETERS angstrom
     -1.2280000000      -2.1269583917      -0.0000000000
     -1.2280000000       2.1269583917       0.0000000000
      0.0000000000       0.0000000000      -6.6960000000
EOF

    # run the pw.x calculation
    pw.x -in pw.graphite.scf.in > pw.graphite.scf.out

    # collect the ecutwfc and total-energy from the pw.graphite.scf.out output-file
    grep -e 'number of k points' -e ! pw.graphite.scf.out | \
        awk -v kPoints="$kpoints" \
            '/number/         {nK=$(NF)}
             /!/              {print kPoints, nK, $(NF-1)}' >> graphite.etot_vs_kPoints

done

# plot the result

#gnuplot plot.gp
