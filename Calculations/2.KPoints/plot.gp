set terminal png
set output 'graphite.kPoints.png'
set grid
set format y "%6.3f"
set ylabel "Total energy (Ry)"
set xlabel "KPoints"

plot 'graphite.etot_vs_kPoints' using 2:3 w linespoints lw 3 pt 5 #ps 1.2
pause -1

