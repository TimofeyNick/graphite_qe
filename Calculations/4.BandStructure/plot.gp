# insert here the correct Fermi energy:
Efermi=5.6599
# .. and uncomment the next line:
set xzeroaxis lt -1

set terminal png enhanced font "Times New Roman,30.0" size 1500,1100
set output 'graphite.bands.png'
set grid xtics lt -1 lw 1
set format y "%5.1f"
set format x ""
set ylabel "Energy (Ry)"
unset xlabel

set xtics ("M" 0.0000, "{/Symbol G}" 0.57733, "K" 1.24397, "M" 1.5773)

#plot [0:3.3320] 'Si.bands.dat.gnu' u 1:($2-Efermi) notitle w linespoints lw 2 pt 6
set yr [-10:20]
plot 'graphite.bands.dat.gnu' u 1:($2-Efermi) notitle w lines lw 2#linespoints lw 2 #pt 6
pause -1

