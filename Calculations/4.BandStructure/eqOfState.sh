#!/bin/sh
# reminder: from now on, what follows the character # is a comment


# delete the graphite.etot_vs_ecut if it exists
rm -f graphite.etot_vs_alat

# loop over ecutwfc value
for istep in 0 1 2 3 4 5 6 7 8 9 10 11 12 #2 #3 4 5 6 7 8 #6 8 10 12
do
   alat=$(awk "BEGIN {print $istep*0.01+4.60; exit}")
   echo "Running for alat = $alat ..."

    # self-congraphitestent calculation
    cat > pw.graphite.scf.in << EOF
 &CONTROL
   calculation = 'scf',
   prefix='graphite',
   pseudo_dir = './pseudo/'
 /
 &SYSTEM
   ibrav = 0 
   !celldm(1) = 4.641167  
   celldm(1) = $alat 
   nat = 4
   nosym = .false.
   ntyp = 1
   ecutrho = 3.600d+02    
   ecutwfc = 40, 
 /
 &ELECTRONS
 /
ATOMIC_SPECIES
   C 12.0107 C.pbe-n-kjpaw_psl.1.0.0.UPF
ATOMIC_POSITIONS crystal
   C  0.0000000000       0.0000000000       0.0000000000
   C  0.0000000000       0.0000000000       0.5000000000
   C  0.6666666667       0.3333333333       0.0000000000
   C  0.3333333333       0.6666666667       0.5000000000
K_POINTS automatic
   7 7 7 0 0 0
CELL_PARAMETERS alat
         -0.500000  -0.866025  -0.000000
         -0.500000   0.866025   0.000000
          0.000000   0.000000  -2.726384
EOF

    # run the pw.x calculation
    pw.x -in pw.graphite.scf.in > pw.graphite.scf.out

    # collect the ecutwfc and total-energy from the pw.graphite.scf.out output-file
    grep -e 'unit-cell volume' -e ! pw.graphite.scf.out | \
       awk  '/unit-cell/        {alat1=$(NF-1)}
             /!/              {print alat1, $(NF-1)}' >> graphite.etot_vs_alat

done

# plot the result

#gnuplot plot.gp
