import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import curve_fit

# Reading file
iFile = 'graphite.etot_vs_alat'
df = pd.read_csv(iFile, header=None, sep='\s+', names=['V', 'E'])

# Plotting results
def func(x, a, b, c):
    return a*x**2 + b*x + c
ydata = df['E']
xdata = df['V']
popt, pcov = curve_fit(func, xdata, ydata)
plt.plot(xdata, ydata, 'o', label='calc')

plt.plot(xdata, func(xdata, *popt), 'r-',
         label='fit a*V^2 + b*V + c: \n a=%.1e, b=%.1e, c=%.1e' % tuple(popt))

plt.xlabel('V, (a.u.)^3', fontsize=15)
plt.ylabel('E per atom, Ry', fontsize=15)
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('E_V.png',dpi=200)
plt.show()
print('Plot is done!')

# Calculations
B1 = popt[1]
au_to_m = 5.29177210903*10**(-11) 
Ry_to_Dj = 2.1798723611035*10**(-18)
K = - B1 * Ry_to_Dj / au_to_m**3 / 10**9 #in GPa
print(f'K = {K:.3f} GPa')

B2 = popt[0]
V0 = - B1 / (2*B2)
au_to_A = 5.29177210903*10**(-11)*10**(10)
V0 = V0 * au_to_A**3
print(f'V0 = {V0:.3f} A^3')